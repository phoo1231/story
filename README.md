﻿
﻿นิทานอีสป เรื่อง กระต่ายกับเต่า (katai kub toi)
ผู้แต่ง : อีสป (E-sob Ja)
วันหนึ่ง กระต่ายป่าหัวเราะเต่าว่าขาสั้นและเดินเชื่องช้า เมื่อเต่าได้ยินจึงท้าทายกลับไปว่า "ถึงเจ้าจะวิ่งเร็ว 
แต่ข้าคิดว่าถ้าเราลองมาแข่งกัน ข้าจะต้องเอาชนะเจ้าได้แน่ เพราะยังไงเจ้าก็ต้องหลับอยู่ใต้ต้นมะม่วงแน่นอน อิอิ " แต่กระต่ายป่ากลับมั่นใจว่าเต่าไม่มีทางเอาชนะมันได้แน่นอน
 มันจึงตอบตกลง โดยให้สุนัขจิ้งจอกมาเป็นผู้ตัดสิน เมื่อถึงวันแข่งขัน กระต่ายป่ากับเต่าก็มาวิ่งแข่งกัน เต่าค่อย ๆ
 เดินอย่างเชื่องช้า แต่สม่ำเสมอและไม่หยุดพัก ส่วนกระต่ายป่าวิ่งนำเต่าไปมาก 
ก็ชะล่าใจและคิดในใจว่า "เต่ากากๆอย่างมึงอะ ยังไงก็ไม่มีทางวิ่งแซงตรูได้หรอกนะไอหนู" จนเวลาผ่านไปกระต่ายป่าสะดุ้งตื่นด้วย
มองซ้าย มองขวาไม่เห็นเต่า จึงรีบวิ่งอย่างสุดแรงแต่ก็ช้าไปเสียแล้ว เต่าได้มาถึงเส้นชัยก่อนและกำลังนอนพักผ่อนอย่างสบาย
RIP

:: นิทานเรื่องนี้สอนให้รู้ว่า ::
ความพยายามอยู่ที่ไหน ความสำเร็จอยู่ที่นั่น

:: พุทธภาษิต ::
อนิพฺพินฺทิยการิสฺส สมฺมทตฺโถ วิปจฺจติ. 
ประโยชน์ย่อมสำเร็จโดยชอบแก่ผู้ทำโดยไม่เบื่อหน่าย . 


Aesop’s Fables: The Donkey and the Dog

Old donkey lived in a snug, warm barn.
He always had plenty of oats and hay to eat.
His farmer was kind and never worked him too hard.
Still, Donkey was not content.
You see, the farmer also had a dog, a dainty little ball of fluff.
This dog performed silly tricks and danced around on two legs.
The farmer fed her tidbits and allowed her to stay in the farmhouse and sleep on his lap.
“It’s not fair!” Donkey told himself.
“That dog is the farmer’s darling, all because she jumps about in that ridiculous way.
Yet I have to pull a heavy cart back and forth to the fields all day long.
Well, I can be amusing, too!”
Into the farmer’s house clomped the donkey. He began to whirl about.
Have you ever seen a big, awkward donkey trying to dance around on two hooves?
It is not a pretty sight. Lamps and dishes went flying,
chairs and tables crashed, and pictures flew off the walls.
Then Donkey began to roll over on the carpet and do other silly tricks,
just as he had seen the little dog do.
The farmer was furious! He grabbed a rope, threw it over Donkey’s neck,
and dragged the surprised beast out to the barn.
There he tied the donkey up in his stall.
“No more oats for you!” he declared.
“And from now on, you’ll have to work twice as hard!
Perhaps then you will be too tired to make mischief!”

Moral of the story
“Don’t try to be someone you’re not. Instead, be content with who you are”


I LOVE YOU~
I MISS YOU

YOYO